\usetheme{default}
\usecolortheme{default}

\usepackage{graphicx,physics,braket,amssymb,amsmath,tabularx,fontspec,hyperref,xcolor}

\setsansfont[
  Path=fonts/,
  Ligatures=TeX,
  BoldFont = SourceSansPro-Bold,
  ItalicFont = SourceSansPro-It,
  BoldItalicFont = SourceSansPro-BoldIt
  ]{SourceSansPro-Regular}

\newcommand{\nodagger}{\phantom{\dagger}}
\newcommand{\noprime}{\phantom{\prime}}
\newcommand{\nostar}{\phantom{*}}

\newcommand{\normalFigureHeight}{7em}

\definecolor{myRed}{HTML}{ff0c00}
\definecolor{myGreen}{HTML}{019301}

\def\tabularxcolumn#1{m{#1}}
\newcolumntype{C}{>{\centering\arraybackslash}X}

\graphicspath{{images/}}

\title{Simulating continuous symmetry breaking with cold atoms in optical cavities}
\author{Ryan Moodie}
% \institute{\texorpdfstring
% {Supervisors: Jonathan Keeling and Kyle Ballantine\\\vspace{1em}PH5103\\MPhys Project in Theoretical Physics}
% {Supervisors: Jonathan Keeling and Kyle Ballantine. PH5103 MPhys Project in Theoretical Physics}
% }
\date{May 2017}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\section[Background]{Background}

\begin{frame}
\frametitle{Dicke model}
    \only<1| handout:0>{
    \begin{equation*}
            \hat{H} = 
            \omega \hat{a}^{\dagger} \hat{a}
            + \sum_{i = 1}^{N} \bigg \{ \omega_{0} \hat{s}^{z}_{i}
            + g \Big [ \left ( \hat{a}^{\dagger} \hat{s}^{-}_{i}
                + \hat{a}^{\dagger} \hat{s}^{+}_{i} \right )
            + H.c. \Big ] \bigg \}
    \end{equation*}}
    \only<2->{\begin{equation*}
            \hat{H} = 
            \omega \textcolor{blue}{\hat{a}^{\dagger} \hat{a}}
            + \sum_{i = 1}^{N} \bigg \{ \omega_{0} \textcolor{myRed}{\hat{s}^{z}_{i}}
            + g \Big [ \left ( \textcolor{blue}{\hat{a}^{\dagger}} \textcolor{myRed}{\hat{s}^{-}_{i}}
                + \textcolor{blue}{\hat{a}^{\dagger}} \textcolor{myRed}{\hat{s}^{+}_{i}} \right )
            + H.c. \Big ] \bigg \}
    \end{equation*}}
    \begin{itemize}
        \item <2-> \textcolor{myRed}{Two-level atoms} interacting with \textcolor{blue}{single light mode}
        \item <3-> $\mathbb{Z}_{2}$ symmetry
        \item <4-> Superradiant phase transition
        \item <5-> Open system experiments
    \end{itemize}
    \begin{columns}
    \column{0.5\linewidth}
    \centering
    \onslide<2->{
    \begin{figure}\href{run:vectors/diagram_edit.pdf}{\XeTeXLinkBox{\includegraphics[width=\linewidth]{diagram_edit}}}
    \end{figure}
    {\tiny \phantom{B}}}
    \column{0.5\linewidth}
    \centering
    \onslide<5->{
    \begin{figure}
    \href{run:images/chequerboard.png}{\XeTeXLinkBox{\includegraphics[width=0.8\linewidth]{chequerboard}}}
    \end{figure}
    \tiny {Keeling et al., Phys. Rev. Lett. \textbf{105}, 043001 (2010)}}
    \end{columns}
\end{frame}

\note{g fixed in closed system}

\begin{frame}
\frametitle{Nonequilibrium model}
\begin{equation*}
            \hat{H} = \sum_{i=1}^{N} \bigg \{ \left ( \omega + U \hat{s}^{z}_{i} \right ) \hat{a}^{\dagger} \hat{a} + \omega_{0} \hat{s}^{z}_{i}
            + \Big [ \left ( \only<1| handout:0>{g \hat{a}^{\dagger} \hat{s}^{-}_{i}}\only<2->{\textcolor{magenta}{g \hat{a}^{\dagger} \hat{s}^{-}_{i}}} 
            + \only<1| handout:0>{g^{\prime} \hat{a}^{\dagger} \hat{s}^{+}_{i}}\only<2->{\textcolor{cyan}{g^{\prime} \hat{a}^{\dagger} \hat{s}^{+}_{i}}} \right ) + H.c. \Big ] \bigg \}
        \end{equation*}
\begin{columns}
\onslide<2->{\column{0.5\textwidth}
\begin{itemize}
    \item Effective two-level systems
\end{itemize}
\centering
\begin{figure}
\href{run:images/raman.png}{\XeTeXLinkBox{\includegraphics[width=0.8\linewidth]{raman}\vspace{-1em}}}
\end{figure}
{\tiny Dimer et al., Phys. Rev. A \textbf{75}, 013804}}
\column{0.5\textwidth}
    \begin{itemize}
        \item <3-> Energy scales: 
        \begin{itemize}
         \item <4-> frequency shifts
         \item <5-> pumping strengths
        \end{itemize}
        \item <6-> Stark shift feedback term: $U$
    \end{itemize}
    \centering
    \onslide<7->{
    \begin{figure}\href{run:images/phase-diagram.png}{\XeTeXLinkBox{\includegraphics[width=\linewidth]{phase-diagram}}}
    \end{figure}
    \vspace{-1em}
    \tiny Baumann et al., Nature \textbf{464}, 1301--1306 (2010)}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Extension to two modes}
\begin{multline*}
    \hat{H} = 
    \sum_{i=1}^{N} \bigg \{ \left ( \omega_{a} + U_{a} \hat{s}^{z}_{i} \right ) \hat{a}^{\dagger} \hat{a} 
    + \left ( \omega_{b} + U_{b} \hat{s}^{z}_{i} \right ) \hat{b}^{\dagger} \hat{b} 
    + \omega_{0} \hat{s}^{z}_{i} \\
    + \left [ \Big ( 
        \only<1| handout:0>{g_{a} \hat{a}^{\dagger} \hat{s}^{-}_{i}} \only<2->{\textcolor{magenta}{g_{a} \hat{a}^{\dagger} \hat{s}^{-}_{i}}} 
        + \only<1| handout:0>{g_{b} \hat{b}^{\dagger} \hat{s}^{+}_{i}} \only<2->{\textcolor{red}{g_{b} \hat{b}^{\dagger} \hat{s}^{+}_{i}}} 
        \visible<7>{+ \textcolor{cyan}{g_{a}^{\prime} \hat{a}^{\dagger} \hat{s}^{+}_{i}} 
    + \textcolor{myGreen}{g_{b}^{\prime} \hat{b}^{\dagger} \hat{s}^{-}_{i}}} \Big ) + H.c. \right ] \bigg \}
\end{multline*}
\vspace{-1em}
\begin{columns}
\column{0.6\textwidth}
    \begin{itemize}
        \item <3-> $U(1)$ symmetry 
        \item <4-> Overlapped cavities {\small[L\'eonard et al., Nature \textbf{543}, 87--90 (2017)]}
        \item <5-> Single multimode cavity
        \begin{itemize}
            \item <6-> Extended range of symmetry
        \end{itemize}
        \item <7-> General model
    \end{itemize}
\column{0.4\textwidth}
\begin{figure}
\only<1| handout:0>{\includegraphics[height=10em]{raman_blank}}
\only<2-6| handout:0>{\href{run:images/raman3.png}{\XeTeXLinkBox{\includegraphics[height=10em]{raman3}}}}
\only<7>{\href{run:images/raman2.png}{\XeTeXLinkBox{\includegraphics[height=10em]{raman2}}}}
\end{figure}
\end{columns}
\end{frame}

\note{Mention at start: a,b opposite way around. Also, the U(1) symmetry for g'=0 is different to that in the Esslinger group nature paper. (The Esslinger group paper has two different internal atomic degrees of freedom coupling to different cavity modes)}

\begin{frame}
\frametitle{Overview}
\pause
    \begin{itemize}[<+->]
        \item Dicke model 
        \begin{itemize}[<+->]
        	\item superradiance
        	\item $\mathbb{Z}_{2}$ symmetry
        	\item Cavity QED experiments
    	\end{itemize}
        \item Extension: 
        \begin{itemize}[<+->]
	        \item Two-mode-cavity Dicke model
	        \item $U(1)$ symmetry
	        \item Map out phase diagram
    	\end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Outline}
\tableofcontents[currentsection]
\end{frame}

\begin{frame}
\frametitle{Outline}
\tableofcontents
\end{frame}

\section[General Methods]{General Methods}

\begin{frame}
\frametitle{Outline}
\tableofcontents[currentsection]
\end{frame}

\begin{frame}
\frametitle{Equations of motion}
    \begin{itemize}[<+->]
        \item Large scale $N$: mean-field theory
        \item Variables: \begin{align*}
            \alpha =& \braket{\hat{a}} & 
            \beta =& \braket{\hat{b}} & 
            S^{\pm} =& \left \langle \sum_{i=1}^{N} \hat{s}^{\pm}_{i} \right \rangle & 
            S^{z} =& \left \langle \sum_{i=1}^{N} \hat{s}^{z}_{i} \right \rangle
        \end{align*}
        \item Lindblad equation and Hamiltonian give dynamics:
        \begin{equation*}
            \dot{\hat{\rho}} = - i \left[\hat{H}, \hat{\rho}\right] + \frac{\kappa_{a}}{2} \mathcal{L}(\hat{a}) + \frac{\kappa_{b}}{2} \mathcal{L}(\hat{b})
        \end{equation*}
        \item $\braket{\hat{x}} = Tr(\hat{\rho} \, \hat{x}) 
        \quad \rightarrow \quad 
        \frac{d}{d t} \braket{\hat{x}} = Tr(\dot{\hat{\rho}} \, \hat{x})$
        \vspace{0.5em}
        \item Semiclassical approximation: 
        $\left \langle \hat{x} \, \hat{y} \right \rangle = \left \langle \hat{x} \right \rangle \left \langle \hat{y} \right \rangle$
    \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Equations of motion}
\begin{itemize}[<+->]
    \item With:
\begin{align*}            
\textcolor{myRed}{\tilde{\omega}_{a}} &\textcolor{myRed}{= \omega_{a} + U_{a} S^{z}} \\
\textcolor{myGreen}{\tilde{\omega}_{b}} &\textcolor{myGreen}{= \omega_{b} + U_{b} S^{z}} \\
\textcolor{cyan}{\tilde{\omega}_{0}} \, & \textcolor{cyan}{= \omega_{0} + U_{a} \abs{\alpha}^{2} + U_{b} \abs{\beta}^{2}} \\
\textcolor{magenta}{\gamma} \, & \textcolor{magenta}{= g_{a} \alpha^{*} + {g_{a}^{\prime}}^{*} \alpha + {g_{b}}^{*} \beta + g_{b}^{\prime} \beta^{*}}
\end{align*}
        \item equations of motion are:
        \begin{align*}
            \dot{\alpha} &= - i \bigg [ \left ( \textcolor{myRed}{\tilde{\omega}_{a}} - i \frac{\kappa_{a}}{2} \right ) \alpha 
            + g_{a} S^{-} + g_{a}^{\prime} S^{+} \bigg ] \\
            \dot{\beta} &= - i \bigg [ \left ( \textcolor{myGreen}{\tilde{\omega}_{b}} - i \frac{\kappa_{b}}{2} \right ) \beta 
            + g_{b} S^{+} + g_{b}^{\prime} S^{-} \bigg ] 
        \end{align*}
        \noindent\begin{tabularx}{\linewidth}{X X}
        \vspace{-2.7em}{\begin{align*}
            \dot{S^{+}} = i \Big ( \textcolor{cyan}{\tilde{\omega}_{0}} S^{+}   - 2 \textcolor{magenta}{\gamma} S^{z} \Big ) 
        \end{align*}} &
        \vspace{-2.7em}{\begin{align*}
            \dot{S^{z}} = i \Big ( \textcolor{magenta}{\gamma} S^{-} - c.c. \Big ) 
        \end{align*}}
        \end{tabularx}
    \end{itemize}
        \end{frame}

\begin{frame}
\frametitle{Steady state solutions}
    \begin{itemize}
        \item <1-> $\ket{\alpha,\beta,S^{+},S^{z}} = \ket{\alpha_{0},\beta_{0},S^{+}_{0},S^{z}_{0}}$
    \end{itemize}
    \begin{itemize}
        \item <2-> Steady state equations:
        \vspace{-2em}
        \end{itemize}
            \onslide<3->{\noindent\begin{tabularx}{\linewidth}{X X}
            {
            \begin{align*}
                \alpha_{0} &= - \frac{g_{a}^{\noprime} S_{0}^{-} + g_{a}^{\prime} S_{0}^{+}}
                {\textcolor{myRed}{\tilde{\omega}_{a}} - \frac{1}{2} i \kappa_{a}} 
            \\
                \textcolor{cyan}{\tilde{\omega}_{0}} S^{+}_{0} &= 2 \textcolor{magenta}{\gamma} S^{z}_{0} 
            \end{align*}
            } 
            &
            {
            \begin{align*}
                \beta_{0} &= - \frac{g_{b}^{\noprime} S_{0}^{+} + g_{b}^{\prime} S_{0}^{-}}
                {\textcolor{myGreen}{\tilde{\omega}_{b}} - \frac{1}{2} i \kappa_{b}}
            \\
                0 &= \textcolor{magenta}{\gamma} S^{-}_{0} - c.c.                   
            \end{align*}
            }
            \end{tabularx}}
    \begin{itemize}
    	\item <4-> Solutions:
    \begin{itemize}
        \item <5-> 
        \noindent\begin{tabularx}{\linewidth}{p{6em} X}
        Normal & $\Downarrow \; = \ket{0,0,0,-\frac{N}{2}}$ 
        \end{tabularx}
        \item <6-> 
        \noindent\begin{tabularx}{\linewidth}{p{6em} X}
        Inverted & $\Uparrow \; = \ket{0,0,0,\frac{N}{2}}$ 
        \end{tabularx}
        \item <7-> 
        \noindent\begin{tabularx}{\linewidth}{p{6em} X}
        Superradiant & $\text{SR} = \ket{\alpha \neq 0,\beta \neq 0,S^{+} \neq 0, \abs{S^{z}} < \frac{N}{2}}$ 
        \end{tabularx}
    \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Linear stability \only<6->{and phase diagrams}}
    \begin{itemize}[<+->]
        \item $\ket{\alpha_{0} + \delta \alpha, \beta_{0} + \delta \beta, S^{+}_{0} + \delta S^{+}, S^{z}_{0} + \delta S^{z}}$
        \item Small fluctuations: linear order
        \item $\delta \alpha = a_{1} e^{-i \lambda t} + a_{2}^{*} e^{i \lambda^{*} t}$
        \item $\lambda \mathbf{v} = \mathbf{M} \, \mathbf{v}$
        \begin{itemize}
        \item Any $\Im(\lambda) > 0 \Rightarrow$ unstable
    \end{itemize}
    \end{itemize}
    \begin{itemize}[<+->]
        \item Phase: possible and stable
        \item Closed system: ground state ($T=0$)
        \item Open system: attractors
        \begin{itemize}
        \item Multiple stable attractors
        \item No stable attractors: ``$?$''
        \item Limit cycles
    \end{itemize}
    \end{itemize}
\end{frame}



\section[Limiting cases]{Limiting cases}

\begin{frame}
\frametitle{Outline}
\tableofcontents[currentsection]
\end{frame}

\subsection[\texorpdfstring{$g_{a}=g_{b}=g_{a}^{\prime}=g_{b}^{\prime}=g$}{g prime equals g}]{\texorpdfstring{$g_{a}=g_{b}=g_{a}^{\prime}=g_{b}^{\prime}=g$}{g prime equals g}}

\begin{frame}
\frametitle{Outline}
\tableofcontents[currentsection, currentsubsection]
\end{frame}

\begin{frame}
\frametitle{\texorpdfstring{$g_{a}=g_{b}=g_{a}^{\prime}=g_{b}^{\prime}=g$}{g prime equals g}}
\begin{equation*}
    \hat{H} = 
    \textcolor{myRed}{\tilde{\omega}_{a}} \hat{a}^{\dagger} \hat{a} 
    + \textcolor{myGreen}{\tilde{\omega}_{b}} \hat{b}^{\dagger} \hat{b} 
    + \omega_{0} \hat{S}^{z} 
    + 2 g \bigg [ \Big ( 
        \hat{a}
        + \hat{b}
         \Big ) + H.c. \bigg ] \hat{S}^{x}
\end{equation*}
    \begin{itemize}
    	\vspace{-1em}
    	\item <2-> Hamiltonian:
    	\begin{itemize}
        \item <2-> Simple
        \item <3-> Test: single mode mapping
    \end{itemize}
        \item <4-> Steady state:
        \vspace{-1.5em}
    \end{itemize}
\onslide<4->{\noindent\begin{tabularx}{\linewidth}{X X}
{\begin{align*}
    \alpha_{0} &= - \frac{2 g S^{x}_{0}}{\textcolor{myRed}{\tilde{\omega}_{a}} - i \frac{\kappa_{a}}{2}} \\
    \textcolor{cyan}{\tilde{\omega}_{0}} S^{+}_{0}
    &= 2 g \Big [ \left ( \alpha_{0} + \beta_{0} \right ) + c.c. \Big ] S^{z}_{0} 
\end{align*}} &
{\begin{align*}
    \beta_{0} &= - \frac{2 g S^{x}_{0}}{\textcolor{myGreen}{\tilde{\omega}_{b}} - i \frac{\kappa_{b}}{2}} \\
    0 &= \Big [ \left ( \alpha_{0} + \beta_{0} \right ) + c.c. \Big ] S^{y}_{0}
    \end{align*}}
\end{tabularx}}
    \begin{itemize}
        \vspace{-1em}
        \item <5-> Solutions:
        \begin{itemize}
            \item <6-> 
            \noindent\begin{tabularx}{\linewidth}{p{4em} C C}
            Normal & $\alpha_{0}=\beta_{0}=0$ &
            \end{tabularx}
            \item <7-> 
            \noindent\begin{tabularx}{\linewidth}{p{4em} C C}
            SRA & $\alpha_{0},\beta_{0} \neq 0$ & $S^{y}_{0}=0$
            \end{tabularx}
            \item <8-> 
            \noindent\begin{tabularx}{\linewidth}{p{4em} C C}
            SRB & $\alpha_{0},\beta_{0} \neq 0$ & $[ \alpha_{0} + \beta_{0} ] + c.c. = 0$
            \end{tabularx}
        \end{itemize}
        \item <9-> $\mathbb{Z}_{2}$ symmetry breaking
    \end{itemize}
\end{frame}

\begin{frame}
\frametitle{\texorpdfstring{$g_{a}=g_{b}=g_{a}^{\prime}=g_{b}^{\prime}=g$}{g prime equals g}}
\begin{itemize}
    \item Single mode mapping
    \begin{itemize}
    	\item <2->  \small{Bhaseen et al., Phys. Rev. A \textbf{85}, 013817 (2012)}
        \item <3-> $\omega_{a} = \omega_{b} = \omega$, $\kappa_{a}=\kappa_{b}=\kappa$, $U_{a}=U_{b}=U$
        \vspace{1ex}
        \item <4-> $\begin{pmatrix}\hat{a}\\\hat{b}\end{pmatrix} = \frac{1}{\sqrt{2}} \begin{pmatrix}1&1\\1&-1\end{pmatrix} \begin{pmatrix}\hat{c}\\\hat{d}\end{pmatrix}$
        \vspace{1ex}
        \item <5-> $\hat{H} = 
                \tilde{\omega} \hat{c}^{\dagger} \hat{c} 
                + \omega_{0} \hat{S}^{z} 
                + 2 \sqrt{2} g \Big ( 
                    \hat{c}
                    + \hat{c}
                     \Big ) \hat{S}^{x}
                + \tilde{\omega} \hat{d}^{\dagger} \hat{d}$
    \end{itemize}
\end{itemize}
    \noindent\begin{tabularx}{\textwidth}{X X X}
    \onslide<6->{\href{run:vectors/equal-gs-u-zero-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{equal-gs-u-zero-omega_v_g-0}}}} &
    \onslide<7->{\href{run:vectors/equal-gs-neg-equal-us-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{equal-gs-neg-equal-us-omega_v_g-0}}}} &
    \onslide<8->{\href{run:vectors/equal-gs-pos-equal-us-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{equal-gs-pos-equal-us-omega_v_g-0}}}} \\
    \centering \onslide<6->{$U=0$} & \centering \onslide<7->{$U>0$} & \centering \onslide<8->{$U<0$}
    \end{tabularx}
\end{frame}

\note{Say: $\Downarrow \rightarrow$ SR on $U=0$}

\begin{frame}
\frametitle{\texorpdfstring{$g_{a}=g_{b}=g_{a}^{\prime}=g_{b}^{\prime}=g$}{g prime equals g}}
\begin{itemize}
    \item Additional axis: $\omega_{a}$--$\omega_{b}$ plane with $g=1$ kHz
\end{itemize}
\noindent\begin{tabularx}{\textwidth}{X X X}
\onslide<2->{\href{run:vectors/equal-gs-u-zero-omega_a_v_omega_b-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{equal-gs-u-zero-omega_a_v_omega_b-0}}}} &
\onslide<3->{\href{run:vectors/equal-gs-neg-equal-us-omega_a_v_omega_b-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{equal-gs-neg-equal-us-omega_a_v_omega_b-0}}}} &
\onslide<4->{\href{run:vectors/equal-gs-pos-equal-us-omega_a_v_omega_b-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{equal-gs-pos-equal-us-omega_a_v_omega_b-0}}}} \\
\centering \onslide<2->{$U=0$} & \centering \onslide<3->{$U>0$} & \centering \onslide<4->{$U<0$}
\end{tabularx}
\begin{itemize}
    \item <5-> $U_{a}=-U_{b}=U$
\end{itemize}
\noindent\begin{tabularx}{\textwidth}{X X}
\onslide<6->{\centering \href{run:vectors/equal-gs-neg-opposite-us-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{equal-gs-neg-opposite-us-omega_v_g-0}}}} &
\onslide<7->{\centering \href{run:vectors/equal-gs-neg-opposite-us-omega_a_v_omega_b-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{equal-gs-neg-opposite-us-omega_a_v_omega_b-0}}}}
\end{tabularx}
\end{frame}

\note{Mention: Variety, ``?'' exists}

\subsection[\texorpdfstring{$g_{a}=g_{b}=g, \; g_{a}^{\prime}=g_{b}^{\prime}=0$}{g prime equals 0}]{\texorpdfstring{$g_{a}=g_{b}=g, \; g_{a}^{\prime}=g_{b}^{\prime}=0$}{g prime equals 0}}

\begin{frame}
\frametitle{Outline}
\tableofcontents[currentsection, currentsubsection]
\end{frame}

\begin{frame}
	\frametitle{\texorpdfstring{$g_{a}=g_{b}=g, \; g_{a}^{\prime}=g_{b}^{\prime}=0$}{g prime equals 0}}
	\begin{equation*}
	    \hat{H} = 
	    \textcolor{myRed}{\tilde{\omega}_{a}} \hat{a}^{\dagger} \hat{a} 
	    + \textcolor{myGreen}{\tilde{\omega}_{b}} \hat{b}^{\dagger} \hat{b} 
	    + \omega_{0} \hat{S}^{z} 
	    + g \bigg [ \Big ( 
	        \hat{a}^{\dagger} 
	        + \hat{b} \Big ) \hat{S}^{-} 
	     + H.c. \bigg ]
	\end{equation*}
    \begin{itemize}
        \item <2-> $U(1)$ symmetry: $\hat{U}=e^{i \theta \hat{G}}$
        \begin{itemize}
        \item <3-> $\hat{U}\hat{H}\hat{U}^{\dagger}=\hat{H}$
        \item <4-> $\hat{G}=\hat{a}^{\dagger}\hat{a}-\hat{b}^{\dagger}\hat{b}+\hat{S}^{z}$
        \item <5-> $[\hat{G},\hat{H}]=0$
    \end{itemize}
        \item <6-> Open system: $\hat{\rho} \rightarrow \hat{U}^{\dagger} \hat{\rho} \hat{U}$ in Lindblad equation 
    \end{itemize}
    \vspace{1em}
    \begin{itemize}
        \item <7-> $\textcolor{myRed}{\tilde{\omega}_{a}}=\textcolor{myGreen}{\tilde{\omega}_{b}} \qquad \kappa_{a}=\kappa_{b}$
        \begin{itemize}
        	\item Solution exists
        \end{itemize}
        \item <8-> Otherwise
        \begin{itemize}
        	\item No stationary superradiant solution
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
\frametitle{\texorpdfstring{$g_{a}=g_{b}=g, \; g_{a}^{\prime}=g_{b}^{\prime}=0$}{g prime equals 0}}
\begin{itemize}
    \item Rotating solution:
    \vspace{-0.5em}
    \begin{equation*}\ket{\alpha,\beta,S^{+},S^{z}}=\ket{\alpha_{0} e^{- i \mu t}, \beta_{0} e^{i \mu t}, S^{+}_{0} e^{i \mu t}, S^{z}_{0}}
    \end{equation*}
    \item <2-> Shifts frequencies: 
    \vspace{-0.5em}
    \begin{align*}
    \tilde{\tilde{\omega}}_{a} &= \textcolor{myRed}{\tilde{\omega}_{a}} - \mu &
    \tilde{\tilde{\omega}}_{b} &= \textcolor{myGreen}{\tilde{\omega}_{b}} + \mu &
    \tilde{\tilde{\omega}}_{0} &= \textcolor{cyan}{\tilde{\omega}_{0}} - \mu 
	\end{align*}
    \vspace{-1em}
    \item <3-> Tune $\mu$:
	\begin{equation*}
	\mu = \frac{1}{2} \left ( \textcolor{myRed}{\tilde{\omega}_{a}} - \textcolor{myGreen}{\tilde{\omega}_{b}} \right ) \qquad \kappa_{a}=\kappa_{b}
	\end{equation*}
	\vspace{-0.5em}
	\item <4-> Spin magnitude conservation:
	\begin{equation*}
	S^{+}_{0} = \sqrt{\frac{N^{2}}{4} - {S^{z}_{0}}^{2}} \, e^{i \phi}
	\end{equation*}
	\item <5-> $U(1)$ symmetry breaking
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{\texorpdfstring{$g_{a}=g_{b}=g, \; g_{a}^{\prime}=g_{b}^{\prime}=0$}{g prime equals 0}}
\begin{itemize}
    \item $U_{a}=U_{b}=0$, $\kappa_{a}=\kappa_{b}=\kappa$ \onslide<3->{\hspace{6em}$g=1$ kHz}
\end{itemize}
\noindent\begin{tabularx}{\textwidth}{X X}
\onslide<2->{
\centering 
\href{run:vectors/zero-g-prime-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{zero-g-prime-omega_v_g-0}}}
}
&
\onslide<3->{
\centering 
\href{run:vectors/zero-g-prime-omega_a_v_omega_b-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{zero-g-prime-omega_a_v_omega_b-0}}}
}
\end{tabularx}
\begin{itemize}
    \item <4-> $U_{a}=-U_{b}=U$, $\kappa_{a}=\kappa_{b}=\kappa$, $\omega_{a}=\omega_{b}=\omega$
    \onslide<5->{\newline $\rightarrow \mu=U S^{z} \rightarrow \tilde{\tilde{\omega}}_{0} = \omega_{0} - U S^{z}$}
\end{itemize}
\noindent\begin{tabularx}{\textwidth}{X X X}
\onslide<6->{
\href{run:vectors/zero-g-prime-u-under-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{zero-g-prime-u-under-omega_v_g-0}}}
}
&
\onslide<7->{
\href{run:vectors/zero-g-prime-u-over-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{zero-g-prime-u-over-omega_v_g-0}}}
}
&
\onslide<8->{
\href{run:vectors/zero-g-prime-u-more-zoom-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=\normalFigureHeight]{zero-g-prime-u-more-zoom-omega_v_g-0}}}
}
\\
\onslide<6->{\centering \vspace{-1em} $U=930$ mHz} & \onslide<7->{\centering \vspace{-1em} $U=950$ mHz} & \onslide<8->{\centering \vspace{-1em} $U=950$ mHz}
\end{tabularx}
\end{frame}

\section[Conclusion]{Conclusion}

\begin{frame}
\frametitle{Outline}
\tableofcontents[currentsection]
\end{frame}

\begin{frame}
\frametitle{Future outlook}
    \begin{itemize}
        \item <1-> Phase diagrams
    \end{itemize}
    \begin{itemize}
        \item <2-> $g_{a} = g_{b} = g, \; g_{a}^{\prime} = g_{b}^{\prime} = i g$
		\begin{multline*}
	    	\hat{H} = 
		    \textcolor{myRed}{\tilde{\omega}_{a}} \hat{a}^{\dagger} \hat{a} 
		    + \textcolor{myGreen}{\tilde{\omega}_{b}} \hat{b}^{\dagger} \hat{b} 
		    + \omega_{0} \hat{S}^{z} \\
		    + \bigg \{ g \left [ 
		    \left ( \hat{a}^{\dagger} + i \hat{b}^{\dagger} \right ) \hat{S}^{-} 
		    + \left ( \hat{b}^{\dagger} + i \hat{a}^{\dagger} \right ) \hat{S}^{+} 
		    \right ] + H.c. \bigg \}
		\end{multline*}
    	\item <3-> $\textcolor{myRed}{\tilde{\omega}_{a}}=\textcolor{myGreen}{\tilde{\omega}_{b}}$: \hspace{1em} $\hat{G}= i \left ( \hat{a}^{\dagger} \hat{b} - \hat{b}^{\dagger} \hat{a} \right ) + \hat{S}^{z}$
        \item <4-> How do $U(1)$ symmetries relate?
    \end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Summary}
    \begin{itemize}
        \item <1-| handout:0> \only<2| handout:0>{Nonequilibrium}\only<3->{Two-mode-cavity} Dicke model \only<1| handout:0>{\phantom{q}}
	\end{itemize}
	\noindent\begin{tabularx}{\linewidth}{X X X X X}
		\onslide<1->{
		\href{run:vectors/diagram_edit.pdf}{\XeTeXLinkBox{\includegraphics[width=\linewidth]{diagram_edit}}} 
		&
		}
		\onslide<2->{
		\href{run:images/phase-diagram.png}{\XeTeXLinkBox{\includegraphics[width=\linewidth]{phase-diagram}}} 
		&
		\href{run:images/chequerboard.png}{\XeTeXLinkBox{\includegraphics[width=\linewidth]{chequerboard}}} 
		&
		\href{run:images/raman.png}{\XeTeXLinkBox{\includegraphics[width=\linewidth]{raman}}} 
		&
		}
		\onslide<3->{
		\href{run:images/raman2.png}{\XeTeXLinkBox{\includegraphics[width=\linewidth]{raman2}}}
		}
	\end{tabularx}
	\vspace{-1em}
	\begin{itemize}
        \item <4-> $g_{a}=g_{b}=g_{a}^{\prime}=g_{b}^{\prime}=g$
    \end{itemize}
	\noindent\begin{tabularx}{\linewidth}{X X X X}
		\onslide<4->{
		\href{run:vectors/equal-gs-u-zero-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/equal-gs-u-zero-omega_v_g-0.png}}} 
		&
		\href{run:vectors/equal-gs-neg-equal-us-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/equal-gs-neg-equal-us-omega_v_g-0.png}}} 
		&
		\href{run:vectors/equal-gs-pos-equal-us-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/equal-gs-pos-equal-us-omega_v_g-0.png}}} 
		&
		\href{run:vectors/equal-gs-neg-opposite-us-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/equal-gs-neg-opposite-us-omega_v_g-0.png}}} 
		\\
		}
		\onslide<5->{
		\href{run:vectors/equal-gs-u-zero-omega_a_v_omega_b-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/equal-gs-u-zero-omega_a_v_omega_b-0.png}}} 
		&
		\href{run:vectors/equal-gs-neg-equal-us-omega_a_v_omega_b-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/equal-gs-neg-equal-us-omega_a_v_omega_b-0.png}}} 
		&
		\href{run:vectors/equal-gs-pos-equal-us-omega_a_v_omega_b-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/equal-gs-pos-equal-us-omega_a_v_omega_b-0.png}}} 
		&
		\href{run:vectors/equal-gs-neg-opposite-us-omega_a_v_omega_b-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/equal-gs-neg-opposite-us-omega_a_v_omega_b-0.png}}}
		}
	\end{tabularx}
    \vspace{-1em}
    \begin{itemize}
        \item <6-> $g_{a}=g_{b}=g, \; g_{a}^{\prime}=g_{b}^{\prime}=0$
    \end{itemize}
    \noindent\begin{tabularx}{\linewidth}{X X X X X}
		\onslide<6->{
		\href{run:vectors/zero-g-prime-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/zero-g-prime-omega_v_g-0.png}}} 
		&
		\href{run:vectors/zero-g-prime-omega_a_v_omega_b-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/zero-g-prime-omega_a_v_omega_b-0.png}}} 
		&
		}
		\onslide<7->{
		\href{run:vectors/zero-g-prime-u-under-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/zero-g-prime-u-under-omega_v_g-0.png}}} 
		&
		\href{run:vectors/zero-g-prime-u-over-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/zero-g-prime-u-over-omega_v_g-0.png}}} 
		&
		\href{run:vectors/zero-g-prime-u-more-zoom-omega_v_g-0.pdf}{\XeTeXLinkBox{\includegraphics[height=4.1em]{smalls/zero-g-prime-u-more-zoom-omega_v_g-0.png}}}
		}
	\end{tabularx}
\end{frame}

\begin{frame}
\frametitle{Outline}
\tableofcontents
\end{frame}

\end{document}
